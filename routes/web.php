<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
// Route::get('/user', 'UserController@index');
// Route::get('/user/create', 'UserController@create'); 
Route::resource('user', 'UserController');  
Route::resource('Estacionamientos', 'EstacionamientosController')->middleware('auth');
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes(['register'=>false,'reset'=>false]);

Route::get('/home', 'HomeController@index')->name('home');
