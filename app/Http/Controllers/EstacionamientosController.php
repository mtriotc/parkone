<?php

namespace App\Http\Controllers;

use App\Estacionamientos;
use Illuminate\Http\Request;

class EstacionamientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['estacionamientos']=Estacionamientos::paginate(20);

        return view('estacionamientos.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estacionamientos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $campos=[
            'Nombre'=>'required|string|max:100',
            'Direccion'=>'required|string|max:191',
            'Latitud'=>'required|numeric|max:90',
            'Longitud'=>'required|numeric|max:90',
            'Espacios'=>'required|numeric|max:1000'
        ];
        $Mensaje=["required"=>'El :attribute es Requerido'];
        $this->validate($request,$campos,$Mensaje);

        $datosEstacionamiento=request()->except('_token');
        Estacionamientos::insert($datosEstacionamiento);
        return redirect('Estacionamientos')->with('Mensaje', 'Estacionamineto agregado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Estacionamientos  $estacionamientos
     * @return \Illuminate\Http\Response
     */
    public function show(Estacionamientos $estacionamientos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Estacionamientos  $estacionamientos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estacionamiento=Estacionamientos::findOrFail($id);

        return view('Estacionamientos.edit',compact('estacionamiento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estacionamientos  $estacionamientos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datosEstacionamiento=request()->except(['_token','_method']);
        Estacionamientos::where('id','=',$id)->update($datosEstacionamiento);
        
        return redirect('Estacionamientos')->with('Mensaje', 'Estacionamiento se ha actualizado');
        // $estacionamiento=Estacionamientos::findOrFail($id);
        // return view('Estacionamientos.edit',compact('estacionamiento'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Estacionamientos  $estacionamientos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Estacionamientos::destroy($id);
        return redirect('Estacionamientos')->with('Mensaje', 'El estacionamiento se ha eliminado');
    }
}
