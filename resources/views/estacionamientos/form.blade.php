<div class="form-group">
    <label for="Nombre" class="control-label">{{'Nombre'}}</label>
    <input type="text" class="form-control" name="Nombre" id="Nombre" 
    value="{{isset($estacionamiento->nombre)?$estacionamiento->nombre:''}}">
</div>

<div class="form-group">
    <label for="Direccion" class="control-label">{{'Dirección'}}</label>
    <input type="text" class="form-control" name="Direccion" id="Direccion" 
    value="{{isset($estacionamiento->direccion)?$estacionamiento->direccion:''}}">
</div>
<div class="form-group">
    <label for="Latitud" class="control-label">{{'Latitud'}}</label>
    <input type="number" class="form-control" step="any" name="Latitud" id="Latitud" 
    value="{{isset($estacionamiento->latitud)?$estacionamiento->latitud:''}}">
</div>
<div class="form-group">
    <label for="Longitud" class="control-label">{{'Longitud'}}</label>
    <input type="number" class="form-control" step="any" name="Longitud" id="Longitud" 
    value="{{isset($estacionamiento->longitud)?$estacionamiento->longitud:''}}">
</div>
<div class="form-group">
    <label for="N° Espacios" class="control-label">{{'Espacios'}}</label>
    <input type="number" class="form-control" name="Espacios" id="Espacios" 
    value="{{isset($estacionamiento->espacios)?$estacionamiento->espacios:''}}">
</div>

<input type="submit" class="btn btn-success" value="{{$Modo=='crear'?'Agregar':'Modificar'}}">

<a class="btn btn-primary" href="{{url('Estacionamientos')}}">Cancelar</a>