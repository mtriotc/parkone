@extends('layouts.app')

@section('content')

<div class="container">
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
        {{Session::get('Mensaje')}}
    </div>
@endif
<a href="{{url('Estacionamientos/create')}}" class="btn btn-primary">Agregar</a>
<br/>
<br/>
<Table class="table table-light table-hover">
    <thead class="thead-light">
        <tr>
            <th>N°</th>
            <th>Nombre</th>
            <th>Dirección</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
        @foreach($estacionamientos as $estacionamiento)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$estacionamiento->nombre}}</td>
            <td>{{$estacionamiento->direccion}}</td>
            <td>
                <a class="btn btn-warning" href="{{url('/Estacionamientos/'.$estacionamiento->id.'/edit')}}">
                    Editar
                </a>
                <form method="post" action="{{ url('/Estacionamientos/'.$estacionamiento->id) }}" style="display:inline">
                {{csrf_field() }}
                {{ method_field('DELETE') }}
                <button class="btn btn-danger" type="submit" onclick="return confirm('¿Esta seguro?');">Borrar</button>
                </form>

            </td>
        </tr>
        @endforeach
    </tbody>

</Table>
{{$estacionamientos->links()}}
</div>
@endsection