<div class="form-group">
    <label for="rut" class="control-label">{{'RUT'}}</label>
    <input type="text" class="form-control" name="rut" id="rut" 
    value="{{isset($user->rut)?$user->rut:''}}">
</div>
<div class="form-group">
    <label for="name" class="control-label">{{'Nombre'}}</label>
    <input type="text" class="form-control" name="name" id="name" 
    value="{{isset($user->name)?$user->name:''}}">
</div>
<div class="form-group">
    <label for="email" class="control-label">{{'e-mail'}}</label>
    <input type="email" class="form-control" step="any" name="email" id="email" 
    value="{{isset($user->email)?$user->email:''}}">
</div>
<div class="form-group">
    <label for="rol" class="control-label">{{'Rol'}}</label>
        <select name="rol" class="form-control"> 
            <option value="1">Administrador</option>    
            <option value="0">Empleado</option>  
        </select>         
</div>
<div class="form-group">
    <label for="estado" class="control-label">{{'Estado'}}</label>
    <select name="estado" class="form-control">    
        <option value="1">Activo</option>    
        <option value="0">inactivo</option>   
    </select>   
</div>
<div class="form-group">
    <label for="estacionamientos_id" class="control-label" >{{'Estacionamiento'}}</label>
        <select name="estacionamientos_id" class="form-control" > 
            <option value="1">Aeromundo</option>    
            <option value="2">Aeromundo 2</option>  
        </select>         
</div>
<div class="form-group">
    <label for="password" class="control-label">{{'Contraseña'}}</label>
    <input type="password" class="form-control" name="password" id="password" 
    value="{{isset($user->password)?$user->password:''}}">
</div>

<input type="submit" class="btn btn-success" value="{{$Modo=='crear'?'Agregar':'Modificar'}}">

<a class="btn btn-primary" href="{{url('user')}}">Cancelar</a>