@extends('layouts.app')

@section('content')

<div class="container">
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
        {{Session::get('Mensaje')}}
    </div>
@endif
<a href="{{url('user/create')}}" class="btn btn-primary">Agregar</a>
<br/>
<br/>
<Table class="table table-light table-hover">
    <thead class="thead-light">
        <tr>
            <th>N°</th>
            <th>RUT</th>
            <th>Nombre</th>
            <th>e-mail</th>
            <th>Rol</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$user->rut}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>Administrador</td>
            <td>
                <a class="btn btn-warning" href="{{url('/user/'.$user->id.'/edit')}}">
                    Editar
                </a>
                <form method="post" action="{{ url('/user/'.$user->id) }}" style="display:inline">
                {{csrf_field() }}
                {{ method_field('DELETE') }}
                <button class="btn btn-danger" type="submit" onclick="return confirm('¿Esta seguro?');">Borrar</button>
                </form>

            </td>
        </tr>
        @endforeach
    </tbody>

</Table>
{{$users->links()}}
</div>
@endsection